package keystore

import (
	"fmt"
	"testing"
)

func TestKeystore(t *testing.T) {
	example_KeystoreNew()
}

func example_KeystoreNew() {
	keystore := NewKeystore("keystore.key", "test")
	err := keystore.Save()
	if err != nil {
		panic(err)
	}
	fmt.Println("Saved keystore")
}
