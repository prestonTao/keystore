package keystore

const (
	MnemonicLang_cn = "cn" //简体中文
	MnemonicLang_en = "en" //英文

)

var (
	CoinType_coin uint32 = 98055361 //本钱包管理的币种编号
	CoinType_net  uint32 = 98055362 //本钱包管理的网络地址编号
	CoinType_dh   uint32 = 98055363 //本钱包管理的dh密钥编号

	ERROR_CODE_file_damage     uint64 = 70001 //文件损坏
	ERROR_CODE_not_find_wallet uint64 = 70002 //未找到指定的钱包
)
