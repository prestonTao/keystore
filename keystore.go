package keystore

import (
	"bytes"
	"gitee.com/prestonTao/utils"
	jsoniter "github.com/json-iterator/go"
	"github.com/tyler-smith/go-bip39/wordlists"
	"os"
	"sync"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

/*
可以导入多个助记词，分别设置密码保存
*/
type Keystore struct {
	filepath     string        //keystore文件存放路径
	AddrPre      string        `json:"-"`         //
	Coinbase     uint64        `json:"coinbase"`  //当前默认使用的收付款地址
	DHIndex      uint64        `json:"dhindex"`   //DH密钥，指向钱包位置
	lock         *sync.RWMutex `json:"-"`         //
	MnemonicLang []string      `json:"-"`         //助记词
	Seed         []byte        `json:"seed"`      //种子
	CheckHash    []byte        `json:"checkhash"` //主私钥和链编码加密验证hash值
	//Addrs        []*AddressInfo `json:"addrs"`     //已经生成的地址列表
	//DHKey         []DHKeyPair        `json:"dhkey"`     //DH密钥
	//NetAddr       *NetAddrInfo       `json:"netaddr"`
	addrMap *sync.Map `json:"-"` //key:string=收款地址;value:*AddressInfo=地址密钥等信息;
	pukMap  *sync.Map `json:"-"` //key:string=公钥;value:*AddressInfo=地址密钥等信息;
	//netAddrPrkTmp ed25519.PrivateKey `json:"-"` //网络地址私钥
	useWallet uint64    `json:"-"`      //当前使用的钱包
	wallet    []*Wallet `json:"wallet"` //导入多个助记词，保存多个钱包
}

func NewKeystore(filepath, addrPre string) *Keystore {
	keys := Keystore{
		filepath:     filepath,          //keystore文件存放路径
		AddrPre:      addrPre,           //
		lock:         new(sync.RWMutex), //
		MnemonicLang: wordlists.English, //
	}
	return &keys
}

/*
使用哪个钱包,从0开始
*/
func (this *Keystore) SetUseWallet(n int) utils.ERROR {
	this.lock.Lock()
	defer this.lock.Unlock()
	if len(this.wallet) < n+1 {
		//下标越界
		return utils.NewErrorBus(ERROR_CODE_not_find_wallet, "")
	}
	return utils.NewErrorSuccess()
}

/*
使用哪个钱包,从0开始
*/
func (this *Keystore) GetUseWallet() *Wallet {
	this.lock.RLock()
	defer this.lock.RUnlock()
	return this.wallet[this.useWallet]
}

/*
获取钱包列表
*/
func (this *Keystore) GetWalletList() []*Wallet {
	this.lock.RLock()
	defer this.lock.RUnlock()
	return this.wallet
}

/*
删除哪个钱包,从0开始
*/
func (this *Keystore) RemoveWallet(n int) utils.ERROR {
	this.lock.Lock()
	defer this.lock.Unlock()
	if len(this.wallet) < n+1 {
		//下标越界
		return utils.NewErrorBus(ERROR_CODE_not_find_wallet, "")
	}
	wallets := make([]*Wallet, len(this.wallet)-1)
	copy(wallets, this.wallet[:n])
	copy(wallets[n:], this.wallet[n+1:])
	if this.useWallet < uint64(n) {
		this.useWallet--
	}
	return utils.NewErrorSuccess()
}

/*
从磁盘文件加载keystore
*/
func (this *Keystore) Load() utils.ERROR {
	if err := utils.RenameTempFile(this.filepath); err != nil {
		return utils.NewErrorSysSelf(err)
	}
	bs, err := os.ReadFile(this.filepath)
	if err != nil {
		return utils.NewErrorSysSelf(err)
	}
	decoder := json.NewDecoder(bytes.NewBuffer(bs))
	decoder.UseNumber()
	err = decoder.Decode(&this)
	if err != nil {
		return utils.NewErrorSysSelf(err)
	}
	this.lock = new(sync.RWMutex)
	this.addrMap = new(sync.Map)
	this.pukMap = new(sync.Map)
	ERR := this.CheckIntact()
	if !ERR.CheckSuccess() {
		return ERR
	}
	// if walletOne.Seed != nil && len(walletOne.Seed) > 0 {
	// 	walletOne.IV = salt
	// }
	// fmt.Println("地址个数=========", len(walletOne.Addrs))
	//for j, one := range this.Addrs {
	//	addrInfo := this.Addrs[j]
	//	// addrStr := one.Addr.B58String()
	//	// addrInfo.AddrStr = addrStr
	//	//存在不是pib44协议地址时直接返回重新创建地址
	//	if addrInfo.Version != version_4 {
	//		this.Addrs = nil
	//		this.DHKey = nil
	//		return nil
	//	}
	//
	//	this.addrMap.Store(utils.Bytes2string(one.Addr), addrInfo)
	//	this.pukMap.Store(utils.Bytes2string(one.Puk), addrInfo)
	//}

	return utils.NewErrorSuccess()
}

/*
从磁盘文件加载keystore
*/
func (this *Keystore) Save() error {
	return utils.SaveJsonFile(this.filepath, this)
}

/*
检查钱包是否完整
*/
func (this *Keystore) CheckIntact() utils.ERROR {
	if this.Seed != nil && len(this.Seed) > 0 {
		if this.CheckHash == nil || len(this.CheckHash) != 32 {
			// fmt.Println("111111111111========", len(this.CheckHash))
			return utils.NewErrorBus(ERROR_CODE_file_damage, "")
		}
	}
	return utils.NewErrorSuccess()
}
