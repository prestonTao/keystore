package keystore

type Wallet struct {
	Nickname  string `json:"nickname"`  //账户昵称
	Seed      []byte `json:"seed"`      //种子
	CheckHash []byte `json:"checkhash"` //主私钥和链编码加密验证hash值
}
